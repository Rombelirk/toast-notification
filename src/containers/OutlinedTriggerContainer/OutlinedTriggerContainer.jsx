import Button from "../../components/Button/Button";
import useNotificationsContext from "../../hooks/use-notifications-context";

const OutlinedTriggerContainer = () => {
  const { showNotification } = useNotificationsContext();
  return (
    <div>
      <Button
        onClick={() =>
          showNotification({
            type: "warning",
            style: "outlined",
            text: "Some warning text here",
          })
        }
        value={"Outlined Warning"}
      />
      <Button
        onClick={() =>
          showNotification({
            type: "success",
            style: "outlined",
            text: "Yor change has been applied!",
          })
        }
        value={"Outlined Success"}
      />
      <Button
        onClick={() =>
          showNotification({
            type: "danger",
            style: "outlined",
            text: "The server responded with an error!",
          })
        }
        value={"Outlined Danger"}
      />
    </div>
  );
};

export default OutlinedTriggerContainer;
