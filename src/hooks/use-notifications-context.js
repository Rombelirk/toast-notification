import { useContext } from "react";
import NotificationsContext from "../context/notifications-context";

export default () => {
  const { showNotification } = useContext(NotificationsContext);
  return { showNotification };
};
