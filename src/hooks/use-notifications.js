import Notification from "../components/Notification/Notification";
import { useState } from "react";

const DEFAULT_NOTIFICATION_DURATION = 6000;

export const useNotifications = ({
  maxVisibleNotifications,
  notificationLifeTime,
}) => {
  const [notifications, setNotifications] = useState(() => []);

  const handleRemoveNotification = (notificationToRemove) => {
    setNotifications((prevNotifications) => {
      return prevNotifications.filter(
        (notification) => notificationToRemove !== notification
      );
    });
  };

  const handleRemoveAllNotifications = () => {
    setNotifications(() => []);
  };

  const getNotificationDuration = (notification) => {
    const duration =
      typeof notification.duration === "number" && notification.duration > 0
        ? notification.duration
        : undefined;
    return duration ?? notificationLifeTime ?? DEFAULT_NOTIFICATION_DURATION;
  };

  const showNotification = (notification) => {
    setNotifications((prevNotifications) => {
      let newNotifications;
      const foundMultipleNotification = prevNotifications.find(
        (notification) => notification.type === "multiple"
      );
      // the notifications already exceeded limit
      if (foundMultipleNotification) {
        notification = {
          type: "multiple",
          count: foundMultipleNotification.count + 1,
        };
        newNotifications = [notification];
      }
      // first notification that exceeds limit
      else if (prevNotifications.length >= maxVisibleNotifications) {
        notification = {
          type: "multiple",
          count: prevNotifications.length + 1,
        };
        newNotifications = [notification];
      } else {
        newNotifications = [...prevNotifications, notification];
      }
      setTimeout(
        () => handleRemoveNotification(notification),
        getNotificationDuration(notification)
      );
      return newNotifications;
    });
  };

  return {
    notifications: (
      <Notification
        handleRemoveNotification={handleRemoveNotification}
        handleRemoveAllNotifications={handleRemoveAllNotifications}
        notifications={notifications}
      />
    ),
    showNotification,
  };
};
