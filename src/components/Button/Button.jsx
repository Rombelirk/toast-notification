import styles from "./styles/button.module.css";

const Button = ({ onClick, value }) => {
  return (
    <button onClick={onClick} className={styles.button}>
      {value}
    </button>
  );
};

Button.defaultProps = {
  value: "",
  onClick: () => null,
};

export default Button;
