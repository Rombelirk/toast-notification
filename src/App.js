import "./App.css";
import { useNotifications } from "./hooks/use-notifications";
import FilledTriggerContainer from "./containers/FilledTriggerContainer/FilledTriggerContainer";
import OutlinedTriggerContainer from "./containers/OutlinedTriggerContainer/OutlinedTriggerContainer";
import NotificationsContext from "./context/notifications-context";

function App() {
  const { notifications, showNotification } = useNotifications({
    maxVisibleNotifications: 3,
    notificationLifeTime: 6000,
  });

  return (
    <NotificationsContext.Provider value={{ showNotification }}>
      <div className="App">
        <FilledTriggerContainer />
        <OutlinedTriggerContainer />
        {notifications}
      </div>
    </NotificationsContext.Provider>
  );
}

export default App;
