import { createContext } from "react";

export default createContext({
  showNotification: () => null,
});
