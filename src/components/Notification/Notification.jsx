import styles from "./styles/notification.module.css";
import slide from "./styles/slide.module.css";
import { ReactComponent as WarningFilled } from "./icons/Warning-filled.svg";
import { ReactComponent as WarningOutlined } from "./icons/Warning-outlined.svg";
import { ReactComponent as SuccessOutlined } from "./icons/Success-outlined.svg";
import { ReactComponent as SuccessFilled } from "./icons/Success-filled.svg";
import { ReactComponent as DangerOutlined } from "./icons/Danger-outlined.svg";
import { ReactComponent as DangerFilled } from "./icons/Danger-filled.svg";
import { ReactComponent as CloseIcon } from "./icons/Close.svg";
import { CSSTransition, TransitionGroup } from "react-transition-group";

const propertiesMap = {
  title: {
    warning: "Warning",
    danger: "Danger",
    success: "Success",
  },
  icon: {
    warning: {
      filled: WarningFilled,
      outlined: WarningOutlined,
    },
    danger: {
      filled: DangerFilled,
      outlined: DangerOutlined,
    },
    success: {
      filled: SuccessFilled,
      outlined: SuccessOutlined,
    },
  },
  style: {
    warning: {
      filled: "filled-warning",
      outlined: "outlined-warning",
    },
    success: {
      filled: "filled-success",
      outlined: "outlined-success",
    },
    danger: {
      filled: "filled-danger",
      outlined: "outlined-danger",
    },
  },
};

const Notification = ({
  notifications,
  handleRemoveNotification,
  handleRemoveAllNotifications,
}) => {
  return (
    <div className={styles["notification-container"]}>
      <TransitionGroup className={styles["notification-list"]}>
        {notifications.map((notification, index) => {
          let Icon;
          if (
            !["warning", "success", "danger", "multiple"].includes(
              notification.type
            ) ||
            !["outlined", "filled"].includes(notification.style)
          ) {
            Icon = WarningOutlined;
          }
          Icon = propertiesMap.icon?.[notification.type]?.[notification.style];
          return (
            <CSSTransition key={index} timeout={500} classNames={slide}>
              {notification.type === "multiple" ? (
                <div
                  className={`${styles.notification} ${
                    styles[propertiesMap.style.warning.outlined]
                  }`}
                >
                  <div className={styles["icon-container"]}>
                    <WarningOutlined />
                  </div>
                  <div className={styles["text-container"]}>
                    <div className={styles["text"]}>
                      You have unread messages
                    </div>
                  </div>

                  <div className={styles["close"]}>
                    <div className={styles["notifications-amount"]}>
                      {notification.count}
                    </div>
                    <CloseIcon onClick={handleRemoveAllNotifications} />
                  </div>
                </div>
              ) : (
                <div
                  key={index}
                  className={`${styles.notification} ${
                    styles[
                      propertiesMap.style[notification.type][notification.style]
                    ]
                  }`}
                >
                  <div className={styles["icon-container"]}>
                    <Icon />
                  </div>
                  <div className={styles["text-container"]}>
                    <div className={styles["title"]}>
                      {propertiesMap.title[notification.type]}
                    </div>
                    <div className={styles["text"]}>{notification.text}</div>
                  </div>
                  <div className={styles["close"]}>
                    <CloseIcon
                      onClick={() => handleRemoveNotification(notification)}
                    />
                  </div>
                </div>
              )}
            </CSSTransition>
          );
        })}
      </TransitionGroup>
    </div>
  );
};

Notification.defaultProps = {
  notifications: [],
  handleRemoveNotification: () => null,
  handleRemoveAllNotifications: () => null,
};

export default Notification;
