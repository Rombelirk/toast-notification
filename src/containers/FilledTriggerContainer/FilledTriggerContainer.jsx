import Button from "../../components/Button/Button";
import useNotificationsContext from "../../hooks/use-notifications-context";

const FilledTriggerContainer = () => {
  const { showNotification } = useNotificationsContext();

  return (
    <div>
      <Button
        onClick={() =>
          showNotification({
            type: "warning",
            style: "filled",
            text: "Some warning text here",
          })
        }
        value={"Filled Warning"}
      />
      <Button
        onClick={() =>
          showNotification({
            type: "success",
            style: "filled",
            text: "This text is absolute nonsense but it is quite long. It is so on purpose to check the notification auto height scaling. Thanks for your attention. Have a nice day!",
          })
        }
        value={"Filled Success"}
      />
      <Button
        onClick={() =>
          showNotification({
            type: "danger",
            style: "filled",
            text: "The operation failed to complete!",
          })
        }
        value={"Filled Danger"}
      />
    </div>
  );
};

export default FilledTriggerContainer;
